
Note1: This module requires content.module, nodereference.module 
       and form_restore.module
          
Note2: Please read CCK documentation (http://drupal.org/node/62459)
       if You are not familiar with CCK and nodereference fields.

Note3: This is my first project so please feel free to point me
       to my mistakes, I'll fix it as soon as possible.
       And sorry for my bad English, I'm not sure that You'r Russian 
       is better ;-P
          
This module adds the ability to reference non-existing (new) nodes
right from nodereference field when You add or edit the node. 
It uses a simple workflow and supports for multi-step chained 
nodes creation - if the content type of new node have nodereference 
fields it is possible to add and reference from it and so on 
(up to 10 steps now supported).

After submit new node You will be returned to the previous (still
not submitted) form with all values You have filled before
and (of coarse) with just added node in the field where 'Add new...'
button has been pressed.

